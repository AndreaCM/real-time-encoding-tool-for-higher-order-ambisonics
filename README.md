=====================
          Introduction
=====================

The final goal of this project is to obtain 3D compositions with as many sound sources as required, placed in all directions in space. For this reason, a real time encoder tool (for third order Ambisonics) which allows to place and move sources in all the room is programmed (using Pure Data). The user will only need to load the mono sound as a "point" or "ring" source, and then they will be able to set the position of the source.
The aim is to get a program adaptable to the needs of the user, which can be modified and customized as required. 


=====================
          Requirements
=====================

- Pd-extended (you can download it here: http://puredata.info/downloads/pd-extended)
- Ambisonics decoder 

=====================
          Configuration
=====================

1. Open the "interface.pd" file. 
The tool programmed consist of one main patch (the interface) which contains different
panels, one large horizontal placed on the top, which controls all sources, and many
vertical panels related with different tracks. The default design contains six different
tracks (one ring source, located on the bottom left, and five point sources). 

2. Customize as required by adding more panels/ removing some
   - To add point sources: add the object "source" (ctrl+1 "source")
   - Ring sources: add the object "ringsource"
   - Source from a DAW: add the object "source_daw" (you need to configure the connections)

3. Play!

=====================

For more information please download the PDF file and read Chapter3 (3.3)

